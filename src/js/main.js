// == START OF CEPAVE JS ==
window.addEventListener('DOMContentLoaded', function (){
  'use strict';

  // offcanvas state
  var trigger  = $('.hamburger'),
      overlay  = $('.overlay'),
      isClosed = false;

  function hamburger_cross() {

    if (isClosed == true) {
      overlay.hide();
      trigger.removeClass('is-open');
      trigger.addClass('is-closed');
      isClosed = false;
    } else {
      overlay.show();
      trigger.removeClass('is-closed');
      trigger.addClass('is-open');
      isClosed = true;
    }
  };

  $("[data-toggle='offcanvas']").click(function () {
    $('#wrapper').toggleClass('toggled');
  });

  // window width state
  function check_device_width() {
    var navbar           = $('.navigation_bar'),
        mobileBreakpoint = 991,
        isMobile = $(window).width() < mobileBreakpoint,
        isDesktop = $(window).width() >= mobileBreakpoint;
    // console.log('isMobile');
    // console.log('!mobile');

    if (isMobile == true) {
      // console.log('isMobile: true');
      navbar.removeClass('is-desktop');
      navbar.addClass('is-mobile');
      navbar.addClass('is-default');
      isDesktop = false;
    } else {
      // console.log('isDesktop: true');
      navbar.removeClass('is-mobile');
      navbar.addClass('is-desktop');
      navbar.addClass('is-close');
      isDesktop = true;
    }
  };

  $(window).resize(function(){
    check_device_width();
  }).resize();

  // START OF: sliders =====
  //all the sliders are configurated via attributes in the markup
  (function() {
    var $sliders = $('.js-slider');
    $sliders.slick();
    $('.slider-for').slick();
    $('.slider-nav').slick();
  })();
  // ===== END OF: sliders

  // Start OF: Scroll show/hide navbar  =====
  var navigation = (function() {
    var bind = function () {
      var currentTop = $(window).scrollTop();
      $(window).scroll(function () {
        var currentTop = $(this).scrollTop();
        var timer = window.setTimeout(function() {
          if (currentTop > 70 || $('body').hasClass('contact')|| $(window).width() < 991 ) {
            $("[data-nav-status='toggle']").addClass('is-scroll');
          } else {
            $("[data-nav-status='toggle']").removeClass('is-scroll');
            $("[data-nav-status='toggle']").removeClass('is-default');
          };
        }, 200);
      });
      if (currentTop > 70 || $('body').hasClass('contact') || $(window).width() < 991 ) {
        $("[data-nav-status='toggle']").addClass('is-default');
      };
    };
    return {
      bind: bind
    }
  }());
  // END OF: Scroll show/hide navbar  =====

  // START OF: filterizr =====
  var filterizr = (function(){
    var bind = function () {
      var $filters = $('.js-filtering-button');
      $filters.on('click', function(event) {
        event.preventDefault();

        $filters.removeClass('state-active').addClass('button--gray');
        $(this).addClass('state-active');
      });
    };

    var init = function () {
      //bind filtering buttons color change:
      bind();

      //init plugin:
      //Default options
      var options = {
        animationDuration: 0.5, //in seconds
        filter: 'all', //Initial filter
        callbacks: {
          onFilteringStart: function() { },
          onFilteringEnd: function() { },
          onShufflingStart: function() { },
          onShufflingEnd: function() { },
          onSortingStart: function() { },
          onSortingEnd: function() { }
        },
        delay: 0, //Transition delay in ms
        delayMode: 'progressive', //'progressive' or 'alternate'
        easing: 'ease-out',
        filterOutCss: { //Filtering out animation
          opacity: 0,
          transform: 'scale(0.75)'
        },
        filterInCss: { //Filtering in animation
          opacity: 1,
          transform: 'scale(1)'
        },
        layout: 'sameSize', //See layouts
        selector: '.filtr-container',
        setupControls: true
      };
      var filterizd = $('.filtr-container').filterizr(options);
      filterizd.filterizr('setOptions', options);
    };
    return {
      init: init
    }
  }());
  // ===== END OF: filterizr =====

  // START OF: content changer =====
  var contentChanger = (function(){
    var $contentTrigger = $('.js-content-trigger');
    var $contentBox = $('.js-content-box');
    var slidingTime = 400;

    var bind = function () {
      $contentTrigger.on('click', function(event) {
        event.preventDefault();
        var contentAttr = $(this).attr('data-content-index');
        $contentTrigger.removeClass('state-active');
        $(this).addClass('state-active');
        $contentBox.slideUp(slidingTime);
        setTimeout(function(){
          $('.js-content-box[data-content-index="' + contentAttr +'"]').slideDown(slidingTime);
        }, slidingTime/2);
      });
    }
    return {
      bind: bind
    }
  }());
  // ===== END OF: content changer

  contentChanger.bind();
  if($('.filtr-container').length > 0){
    filterizr.init();
  };
  navigation.bind();
  trigger.click(function () {
    hamburger_cross();
  });
});
// == END OF CEPAVE JS ==
